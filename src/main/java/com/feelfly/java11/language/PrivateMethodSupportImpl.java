package com.feelfly.java11.language;

public class PrivateMethodSupportImpl implements PrivateMethodSupport {


    @Override
    public void print(String a) {
        System.out.println("调用默认方法");
        mm(a);
        System.out.println(a);
    }

    // default 方法可以不实现
//    @Override
//    public void mm(Object o) {
//
//    }

    public static void main(String[] args) {
        PrivateMethodSupportImpl a=new PrivateMethodSupportImpl();
        a.mm(new SafeVarargsAnno());// 调用 默认方法
        a.print("132132"); // 调用接口方法
        PrivateMethodSupport.dd(new TryWithResources()); // 调用静态方法
    }
}
