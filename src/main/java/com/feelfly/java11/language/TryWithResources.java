package com.feelfly.java11.language;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 更简洁的 try-with-resources
 */
public class TryWithResources {

    /**
     * javaSE 1.7 以前，使用finally 块 来确保关闭资源。
     * 如果readline 和 close 都抛出异常，那么该方法将抛出finally 抛出的异常。
     * @param path
     * @return
     * @throws IOException
     */
    public String readFirstLineFromFileWithFinallyBlock(String path)
            throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            return br.readLine();
        } finally {
            br.close();
        }
    }

    /**
     * javaSE8 以后，Try-with-resources 语句是声明一个或多个资源的 try 语句。资源是程序完成后必须关闭的对象。Try-with-resources 语句确保在语句结束时关闭每个资源。任何实现 java.lang 的对象。包括所有实现 java.io 的对象。可关闭的，可以作为一种资源使用
     * 如果 try 块和 try-with-resources 语句都抛出异常，那么该方法将抛出 try 块中抛出的异常; try-with-resources 块中抛出的异常将被抑制。
     * 在本例中，try-with-resources 语句中声明的资源是 BufferedReader。声明语句出现在 try 关键字之后的括号中。
     * @param path
     * @return
     * @throws IOException
     */
    public String readFirstLineFromFile(String path) throws IOException {
        try (BufferedReader br =
                     new BufferedReader(new FileReader(path))) {
            return br.readLine();
        }
    }

    /**
     * 检索打包在 zip 文件 zipFileName 中的文件名，并创建一个包含这些文件名的文本文件
     * 在此示例中，try-with-resources 语句包含两个声明，它们以分号分隔: ZipFile 和 BufferedWriter。当直接跟随它的代码块终止时，无论是正常的还是由于异常，BufferedWriter 和 ZipFile 对象的 close 方法都会按照这个顺序自动调用。请注意，资源的 close 方法的调用顺序与其创建顺序相反
     * @param zipFileName
     * @param outputFileName
     * @throws java.io.IOException
     */
    public void writeToFileZipFileContents(String zipFileName,
                                                  String outputFileName)
            throws java.io.IOException {

        java.nio.charset.Charset charset =
                StandardCharsets.UTF_8;
        java.nio.file.Path outputFilePath =
                java.nio.file.Paths.get(outputFileName);

        // Open zip file and create output file with
        // try-with-resources statement

        try (java.util.zip.ZipFile zf =
                        new java.util.zip.ZipFile(zipFileName);
                java.io.BufferedWriter writer =
                        java.nio.file.Files.newBufferedWriter(outputFilePath, charset)
        ) {
            // Enumerate each entry
            for (java.util.Enumeration entries =
                 zf.entries(); entries.hasMoreElements();) {
                // Get the entry name and write it to the output file
                String newLine = System.getProperty("line.separator");
                String zipEntryName =
                        ((java.util.zip.ZipEntry)entries.nextElement()).getName() +
                                newLine;
                writer.write(zipEntryName, 0, zipEntryName.length());
            }
        }
    }

    /**
     * 在11中，如果已经有一个资源作为最终或实际上最终的变量，则可以在 try-with-resources 语句中使用该变量，而无需声明新变量。一个“有效的最终”变量是一个其值在初始化后从未更改的变量。
     * @param zipFileName
     * @param outputFileName
     * @throws java.io.IOException
     */
    public void writeToFileZipFileContentsIn11(String zipFileName,
                                           String outputFileName)
            throws java.io.IOException {

        java.nio.charset.Charset charset =
                StandardCharsets.UTF_8;
        java.nio.file.Path outputFilePath =
                java.nio.file.Paths.get(outputFileName);

        // Open zip file and create output file with
        // try-with-resources statement
        java.util.zip.ZipFile zf =
                new java.util.zip.ZipFile(zipFileName);
        java.io.BufferedWriter writer =
                java.nio.file.Files.newBufferedWriter(outputFilePath, charset);
        // 注意下面 直接使用已经声明的“有效的最终”变量
        try (zf;writer) {
            // Enumerate each entry
            for (java.util.Enumeration entries =
                 zf.entries(); entries.hasMoreElements();) {
                // Get the entry name and write it to the output file
                String newLine = System.getProperty("line.separator");
                String zipEntryName =
                        ((java.util.zip.ZipEntry)entries.nextElement()).getName() +
                                newLine;
                writer.write(zipEntryName, 0, zipEntryName.length());
            }
        }
    }

    public static void main(String[] args) throws IOException {
        TryWithResources twr=new TryWithResources();
//        System.out.println(twr.readFirstLineFromFileWithFinallyBlock("D:\\workspaces\\java11tutorial\\ReadMe.md"));
//        System.out.println(twr.readFirstLineFromFile("D:\\workspaces\\java11tutorial\\ReadMe.md"));
//        twr.writeToFileZipFileContents("D:\\workspaces\\java11tutorial\\temp\\temp.zip",
//                "D:\\workspaces\\java11tutorial\\temp\\out.txt");
        // 注意zip 文件名称要是 utf-8 编码的。否则会报错。
        twr.writeToFileZipFileContentsIn11("D:\\workspaces\\java11tutorial\\temp\\temp.zip",
                "D:\\workspaces\\java11tutorial\\temp\\out.txt");
    }
}
