package com.feelfly.java11.language;

/**
 * java11 对接口中的私有方法进行支持，可以非抽象方法在他们之间共享方法。
 */
public interface PrivateMethodSupport {
    /**
     * 常见的 public abstract
     * @param a
     */
    void print(String a);

    /**
     * 支持私有方法了。
     * 解决多个默认方法之间重复代码问题
     * @param obj
     * @return
     */
    private String getName(Object obj){
        return obj.getClass().getName();
    }

    /**
     * 支持静态私有方法了。
     * 解决静态方法之间的代码重复问题
     * @param obj
     * @return
     */
    private static String getTT(Object obj){
        return obj.getClass().getName();
    }

    static void dd(Object o){
        System.out.println(o);
        System.out.println(getTT(o));
    }

    default void mm(Object o){
        System.out.println(o);
        System.out.println(getName(o));
    }

}
