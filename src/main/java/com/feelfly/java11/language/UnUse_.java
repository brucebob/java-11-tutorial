package com.feelfly.java11.language;

public class UnUse_ {

    public static void main(String[] args) {
        //从发行版 9 开始, '_' 为关键字, 不能用作标识符
        /*String _="xxxx";
        System.out.println(_);*/
    }
}
