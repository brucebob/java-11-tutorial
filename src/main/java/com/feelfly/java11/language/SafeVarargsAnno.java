package com.feelfly.java11.language;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @SafeVarargs注解只能用在参数长度可变的方法或构造方法上，且方法必须声明为static或final，否则会出现编译错误。
 * 一个方法使用@SafeVarargs注解的前提是，开发人员必须确保这个方法的实现中对泛型类型参数的处理不会引发类型安全问题，否则可能导致运行时的类型转换异常。
 * 在JDK11 中添加了 在私有实例方法上允许使用@safevarargs 注释。它只能应用于无法重写的方法
 */
public class SafeVarargsAnno {

    public static void main(String[] args) {
        //  不安全的演示,“堆污染”的实例
        List<String> a1= Arrays.asList("1231","213");
        List<String> a2= Arrays.asList("12231","11213");
        unsafeMethod(a1,a2);
    }

    private static void unsafeMethod(List<String>... stss){
        Object[] bjs=stss;
        List<Integer> a= Arrays.asList(24,13);
        bjs[0]=a; // // tmpList是一个List对象（类型已经擦除），赋值给Object类型的对象是允许的（向上塑型），能够编译通过
        String s = stss[0].get(0); // 运行时抛出ClassCastException！
    }

}

