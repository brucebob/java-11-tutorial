package com.feelfly.java11.language;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class LocalVarTypeInfer {


    public static void main(String[] args) throws IOException {
//        在 JDK 10和更高版本中，您可以使用 var 标识符声明带有非空初始化器的局部变量，这可以帮助您编写更容易阅读的代码。
        var url = new URL("http://www.oracle.com/");
        var conn = url.openConnection();
        var reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));

        // 使用初始化器的局部变量声明:
        var list = new ArrayList<String>();    // infers ArrayList<String>
        var stream = list.stream();            // infers Stream<String>
        var path = Paths.get("D:/temp");        // infers Path
        var bytes = Files.readAllBytes(path);  // infers bytes[]

        // 增强的 for 循环索引:
        List<String> myList = Arrays.asList("a", "b", "c");
        for (var element : myList) { // infers String
            System.out.println(element);
        }
        // 在传统 for 循环中声明的索引变量:
        for (var counter = 0; counter < myList.size(); counter++)  { // infers int
            System.out.println(myList.get(counter));
        }

        // Try-with-resources 变量
        try (var input =
                     new FileInputStream("validation.txt")) { // infers FileInputStream
            // TODO
        }

        // 在 JDK 11和更高版本中，你可以用 var 标识符声明一个隐式类型 lambda 表达式的每个形式参数:
//        (var a, var b) -> a + b;
    }
}
