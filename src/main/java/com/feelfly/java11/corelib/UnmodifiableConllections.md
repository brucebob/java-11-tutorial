# Creating Unmodifiable Lists, Sets, and Maps 创建不可修改的列表、集和映射
方便地使用 List、 Set 和 Map 接口上的静态工厂方法，可以轻松地创建不可修改的列表、集和映射。
如果无法添加、删除或替换元素，则集合被视为不可修改。在创建集合的不可修改实例之后，只要对它的引用存在，它就保存相同的数据。
可修改的集合必须维护簿记数据以支持未来的修改。这会增加存储在可修改集合中的数据的开销。不可修改的集合不需要这些额外的簿记数据。由于集合不需要修改，因此集合中包含的数据可以打包得更密集。与包含相同数据的可修改集合实例相比，不可修改集合实例通常消耗的内存要少得多。  
## 用例 Use Cases
使用不可修改集合还是可修改集合取决于集合中的数据。
不可修改的集合提供了空间效率优势，并防止意外修改集合，这可能导致程序不正确地工作。建议对下列情况进行不可修改的收集:
- 从编写程序时已知的常数初始化的集合
- 在程序开始时从已计算的数据或从配置文件等读取的数据初始化的集合
  对于保存在整个程序过程中修改的数据的集合，可修改集合是最佳选择。修改就地执行，因此增量添加或删除数据元素的成本相当低。如果这是通过不可修改的集合完成的，则需要完整的副本来添加或删除单个元素，这通常具有不可接受的开销。
  
## 语法 Syntax
这些新集合的 API 非常简单，特别是对于少量元素。
### 不可修改的列表静态工厂方法
静态工厂方法的 List.of 提供了创建不可修改列表的方便方法。
列表是一个有序的集合，其中允许重复的元素。不允许空值。
List.of()
List.of(e1)
List.of(e1, e2)         // fixed-argument form overloads up to 10 elements
List.of(elements...)    // varargs form supports an arbitrary number of elements or an array
### 不可修改的集合静态工厂方法
静态工厂方法的 Set.of 提供了创建不可修改列表的方便方法。
集合是不包含重复元素的集合。如果检测到重复的条目，则会引发 IllegalArgumentException。不允许使用空值。
Set.of()
Set.of(e1)
Set.of(e1, e2)         // fixed-argument form overloads up to 10 elements
Set.of(elements...)    // varargs form supports an arbitrary number of elements or an array
### 不可修改的地图静态工厂方法
静态工厂方法提供了Map.ofEntries 创建不可修改映射的方便方法。
映射不能包含重复的键。如果检测到重复的key，则会引发 IllegalArgumentException。每个键都与一个值关联。Null 不能用于 Map 键或值。
Map.of()
Map.of(k1, v1)
Map.of(k1, v1, k2, v2)    // fixed-argument form overloads up to 10 key-value pairs
Map.ofEntries(entry(k1, v1), entry(k2, v2),...) // varargs form supports an arbitrary number of Entry objects or an array
如果您有超过10个键值对，那么使用 Map.entry 方法创建映射条目，并将这些对象传递给 Map.ofEntries 方法。例如:
import static java.util.Map.entry;
Map <Integer, String> friendMap = Map.ofEntries(
entry(1, "Tom"),
entry(2, "Dick"),
entry(3, "Harry"),
...
entry(99, "Mathilde"));
### [创建集合的不可修改副本](UnmodifyCollectionCopys.java)
### [从流创建不可修改的集合](UnmodifyCollectionStreams.java)
### 随机迭代顺序
Set 元素和 Map 键的迭代顺序是随机的，从一个 JVM 运行到下一个 JVM 可能不同。这是有意为之的，并且使得识别依赖于迭代顺序的代码变得更加容易。对迭代顺序的无意依赖可能导致难以调试的问题。
jshell> var stringMap = Map.of("a", 1, "b", 2, "c", 3);
stringMap ==> {b=2, c=3, a=1}

jshell> /exit
|  Goodbye

C:\Program Files\Java\jdk\bin>jshell

jshell> var stringMap = Map.of("a", 1, "b", 2, "c", 3);
stringMap ==> {a=1, b=2, c=3}

随机迭代顺序适用于由 Set.of、 Map.of 和 Map.ofEntries 方法以及 toUnmodifiableSet 和 toUnmodifiableMap 收集器创建的集合实例。集合实现(如 HashMap 和 HashSet)的迭代顺序没有改变。
### 关于不可修改的集合
任何从这些不可修改集合中添加、设置或移除元素的尝试都会引发 UnsupportedOperationException。
如果所包含的元素是可变的（集合元素是引用类型的对象），那么这可能会导致集合的行为不一致，或者使其内容看起来更改。
1. [不可修改集合与不可修改视图](UnmodifyCollectionAndView.java)
   不可修改的集合与 Collections.unmodifiable... 方法返回的不可修改视图的行为方式相同。然而，不可修改的集合不是视图。这些是由类实现的数据结构，任何修改数据的尝试都会引发异常。
   如果创建一个 List 并将其传递给 Collections.unmodifiableList 方法，则会得到一个不可修改的视图。基础列表仍然是可修改的，对它的修改可以通过返回的 List 看到，因此它实际上不是不可变的。
   不可修改视图的原因是无法通过调用视图上的方法来修改集合。但是，任何具有对基础集合的引用以及修改该集合的能力的人都可能导致不可修改视图的更改。
     
### 空间效率
Collections方法返回的集合比其可修改的对等集合更节省空间。


