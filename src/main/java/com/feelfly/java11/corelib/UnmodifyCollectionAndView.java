package com.feelfly.java11.corelib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 不可修改集合与不可修改视图
 */
public class UnmodifyCollectionAndView {

    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        System.out.println("原始列表："+list1);
        List<String> unmodifiableList = Collections.unmodifiableList(list1);
        System.out.println("原始列表的视图："+unmodifiableList);
        // unmodifiableList list1的一个视图。不能直接更改视图
//        unmodifiableList.add("e");
        list1.add("f");
        System.out.println("对原始列表修改之后：");

        System.out.println("原始列表："+list1);
        System.out.println("原始列表的视图："+unmodifiableList);


    }
}
