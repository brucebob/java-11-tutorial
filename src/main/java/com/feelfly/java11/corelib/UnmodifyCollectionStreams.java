package com.feelfly.java11.corelib;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 从流创建不可修改的集合
 */
public class UnmodifyCollectionStreams {

    public static void main(String[] args) {
        List<Integer> a=new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        System.out.println("原始集合："+a);
        // 希望保证返回的集合是不可更改的，应该使用 toUnmodifiable-collector 之一
        Set<Integer> results = a.stream().filter((i) -> i % 2 == 0).collect(Collectors.toUnmodifiableSet());
        System.out.println("从流返回的不可修改集合:"+results);
    }
}
