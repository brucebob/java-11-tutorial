package com.feelfly.java11.corelib;

import com.feelfly.java11.domain.People;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 创建集合的不可修改副本
 */
public class UnmodifyCollectionCopys {

    public static void main(String[] args) {
        List<String> a1=new ArrayList<>();
        a1.addAll(getSomeEles());
        a1.add("4");
        System.out.println("原始集合："+a1);
        List<String> snapshot = List.copyOf(a1); // 集合的不可修改副本
        System.out.println("创建的不可修改副本："+snapshot);
        a1.add("5");
        System.out.println("修改后的原始集合："+a1);
        System.out.println("不可修改副本："+snapshot);
        // 下面的修改操作会报错 java.lang.UnsupportedOperationException
//        boolean add = snapshot.add("6");


//        引用类型测试,copyof 不是深度拷贝，依然是引用的内存地址。
        List<People> ps=new ArrayList<>();
        ps.add(new People("小王",1,"男"));
        ps.add(new People("小李",2,"男"));
        ps.add(new People("小卓",3,"男"));
        ps.add(new People("小海",4,"男"));
        System.out.println("原始集合："+ps);
        List<People> snapshot1 = List.copyOf(ps); // 集合的不可修改副本
        System.out.println("创建的不可修改副本："+snapshot1);
        People people = ps.get(0);
        people.setName("笑笑");
        people.setAge(5);
        System.out.println("修改后的原始集合："+ps);
        System.out.println("不可修改副本："+snapshot1);
        System.out.println("原始和不可修改的第一个原始是否是equals:"+ps.get(0).equals(snapshot1.get(0)));

        // 如果原始集合已经不可修改，则 copyOf 方法只返回对原始集合的引用。复制的目的是将返回的集合与对原始集合的更改隔离开来。但是，如果原始集合不能更改，就没有必要复制它
    }

    private static Collection<String> getSomeEles() {
        return List.of("1","2","3");
    }
}
