package com.feelfly.java11.corelib;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UnmodifyList {

    public static void main(String[] args) {
        // in JDK8 创建不可修改的列表
        List<String> stringList = Arrays.asList("a", "b", "c");
        stringList = Collections.unmodifiableList(stringList);
        System.out.println(stringList);
        // in JDK9 or later
        List<String> stringList1 = List.of("a", "b", "c");
        System.out.println(stringList1);
    }
}
