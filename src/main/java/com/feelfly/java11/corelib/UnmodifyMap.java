package com.feelfly.java11.corelib;

import java.util.*;

import static java.util.Map.entry;

public class UnmodifyMap {

    public static void main(String[] args) {
        // in JDK8 创建不可修改的列表
        Map<String, Integer> stringMap = new HashMap<String, Integer>();
        stringMap.put("a", 1);
        stringMap.put("b", 2);
        stringMap.put("c", 3);
        stringMap = Collections.unmodifiableMap(stringMap);
        System.out.println(stringMap);
        // in JDK9 or later
        Map<String, Integer> stringMap1 = Map.of("a", 1, "b", 2, "c", 3);
        System.out.println(stringMap1);

        Map <Integer, String> friendMap = Map.ofEntries(
                entry(1, "Tom"),
                entry(2, "Dick"),
                entry(3, "Harry"),
                entry(99, "Mathilde"));
        System.out.println(friendMap);
    }
}
