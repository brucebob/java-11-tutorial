# XML Catalog API    XML 目录 API
Javase 9引入了一个新的 XML 目录 API，以支持结构化信息标准发展组织(Organization for the Advancement of Structured Information Standards，OASIS) XML 目录，OASIS Standard V1.1。9核心库指南的这一章描述了 API，Java XML 处理器对它的支持，以及使用模式。
XML 目录 API 是实现本地目录的简单 API，JDK XML 处理器的支持使得配置处理器或整个环境更容易利用这一特性。

## XML 目录 API 的用途
XML 目录 API 和 javaxml 处理器为开发人员和系统管理员提供了一个更好地管理外部资源的选项。
XML Catalog API 提供了 OASIS XML Catalogs v1.1的实现，该标准旨在解决外部资源引起的问题。

1. 外部资源引发的问题
   可用性。当资源远程时，XML 处理器必须能够连接到远程服务器。尽管连接性很少是一个问题，但它仍然是应用程序稳定性的一个因素。过多的连接可能会危及持有资源的服务器(例如，文档记录良好的涉及指向 W3C 服务器的过多 DTD 通信量的情况) ，这反过来可能会影响您的应用程序。有关使用 XML Catalog API 解决这个问题的示例，请参见与 XML 处理器一起使用 Catalog)。
   性能。尽管在大多数情况下连接性不是问题，但远程获取仍然会导致应用程序的性能问题。此外，同一个系统上可能有多个应用程序试图解析同一个源，这将浪费系统资源。
   如果应用程序处理不可信的 XML 源，允许远程连接可能会带来安全风险。
   可管理性。如果一个系统处理大量的 XML 文档，那么外部引用的文档，无论是本地文档还是远程文档，都可能成为一个维护难题。
   
2. XML 目录 API 如何解决外部资源引起的问题
   XML 目录 API 和 javaxml 处理器为开发人员和系统管理员提供了一个更好地管理外部资源的选项。
   应用程序开发人员——您可以为应用程序创建一个包含所有外部引用的本地目录，并让 Catalog API 为应用程序解析它们。这不仅避免了远程连接，而且使管理这些资源更加容易。
   系统管理员——您可以为您的系统建立一个本地目录，并将 javavm 配置为指向目录。然后，系统上的所有应用程序可以共享相同的目录，而不需要对应用程序进行任何代码更改，假设它们与 javase 9兼容。要建立目录，您可以利用现有的目录，比如某些 Linux 发行版中包含的目录。
   
## XML 目录 API 接口
通过接口访问 XML 目录 API。
### XML 目录 API 接口
XML 目录 API 定义了以下接口:
- Catalog 接口表示由 XML Catalogs (OASIS Standard V1.1,2005年10月7日)定义的实体目录。Catalog 对象是不可变的。创建之后，Catalog 对象可用于在系统、公共或 uri 条目中查找匹配项。自定义冲突解决程序实现可能会发现通过目录定位本地资源非常有用。
- CatalogFeatures 类包含 catalogapi 支持的所有特性和属性，包括 javax.xml.Catalog.files、 javax.xml.Catalog.defer、 javax.xml.Catalog.prefer 和 javax.xml.Catalog.resolve。
- CatalogManager 类管理 XML 目录和目录解析器的创建。 
- Catalogolver 接口是一个目录解析器，它实现了用于模式验证的 SAX 实体解析器、 staxxmlresolver、 DOM LS LSResourceResolver 和转换 URIResolver。此接口使用目录解析外部引用。

