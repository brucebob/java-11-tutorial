package com.feelfly.java11.corelib;

import java.util.*;

public class UnmodifySet {

    public static void main(String[] args) {
        // in JDK8 创建不可修改的列表
        Set<String> stringSet = new HashSet<>(Arrays.asList("a", "b", "c"));
        stringSet = Collections.unmodifiableSet(stringSet);
        System.out.println(stringSet);
        // in JDK9 or later
        Set<String> stringSet1 = Set.of("a", "b", "c");
        System.out.println(stringSet1);
    }
}
