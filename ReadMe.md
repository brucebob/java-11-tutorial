# 本项目为 JDK11 的使用指南
## 资料准备
### jdk 下载 
1. oracle 版本[下载地址](https://www.oracle.com/java/technologies/javase-downloads.html)
2. openJDK 我使用 adoptopenjdk 版本[下载地址](https://adoptopenjdk.net),其介绍可以见[官网](https://adoptopenjdk.net)说明
### 官方使用指南
1. [Java SE Development Kit 11 Documentation](https://www.oracle.com/java/technologies/javase-jdk11-doc-downloads.html)
2. java [语言和虚拟机规范](https://docs.oracle.com/javase/specs/)
3. [java11 oracle教程](https://docs.oracle.com/en/java/javase/11)
## 简介
### jdk 的内容
1. 可执行文件
   可执行文件(在 bin/子目录中) JRE 文件的实现(JRE)。JRE 包括一个 Java 虚拟机(javavmtm)、类库和其他文件，这些文件支持用 Java 编程语言编写的程序的执行。这个目录还包括一些工具和实用程序，它们可以帮助您开发、执行、调试和记录用 Java 编程语言编写的程序。有关更多信息，请参阅的[工具文档](https://docs.oracle.com/javase/11/tools) 
   
2. 配置文件  
   配置文件(在 conf/子目录中)包含用户可配置选项的文件。这个目录中的文件可以被编辑来改变 JDK 的访问权限，配置安全算法，并设置 Java Cryptography Extension Policy Files，这些文件可能被用来限制 JDK 的加密强度。  
     
3. C 头文件  
   C 头文件(在 include/子目录中) c语言头文件，支持本地代码编程和 Java 虚拟机(JVM) Java本地接口调试器接口。  
     
4. 编译的java 模块  
   编译的 Java 模块(在 jmods/子目录中)由 jlink 用来创建自定义运行时的编译模块。  
     
5. 版权和许可文件  
   版权和许可证文件(在legal/子目录中)许可证和每个模块的版权文件。包括第三者通知，如.Md (markdown)文件。
     
6. 其他库
   其他库(在 lib/子目录中) JDK 需要的其他类库和支持文件。这些文件不是供外部使用的。
     
### jdk 配置
1. 配置 JRE 或 JDK 不被认为是为了重新分发目的而进行的修改。
2. 你可以根据 Java Platform，Standard Edition (javase) Documentation 修改 conf/目录下的文件来配置软件，包括在 conf/security/policy 下选择一个或创建你自己的 Java Cryptography Extension 策略文件。一旦您选择或创建了自己的策略文件，就不需要包含其他策略文件。
3. 根 CA 证书可以通过使用 JDK 的 bin/subdirectory 中可用的 keytool 实用工具添加到位于 lib/security/cacerts 中的 javase 证书文件中或从该文件中删除。
4. 你可以使用 Java 时区更新工具更新包含在 Java JRE 中的时区数据，该工具可在 Java SE 下载页面中找到。
### 无限强度的 Java 密码扩展
本 JRE 中捆绑的默认 JCE 策略文件允许“无限制”的加密强度。
为方便起见，此软件还包含限制加密强度的历史性“有限”强度策略文件。要使用有限强度策略，而不是默认的无限制策略，必须更新“ crypto.policy”Security 属性(位于/conf/Security/java.Security)以指向适当的目录。
## jdk 11 的发行说明
1. [重要的改变和信息](doc/发行说明/重要的改变和信息.md)
2. [新特性和增强](doc/发行说明/新特性和增强.md)
3. [移除的特性和选项](doc/发行说明/移除的特性和选项.md)  
4. [不推荐的特性和选项](doc/发行说明/不推荐的特性和选项.md)
